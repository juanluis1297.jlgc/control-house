#include <Servo.h>          // Incluimos la libreria
Servo gargateservo;         // Crea el gargateservo con las librerias de Servo.h


int estado=0;
int tiempo=100;
int analogo=0;
int sala=13;
int garaje=12;
int cuarto=11;
int azotea=10;
int cocina=9;
int motor=5;
int cochera=4;
void setup(){                   
  gargateservo.attach(6);            // Declaramos el pin del servo
  pinMode(sala,OUTPUT);              // Declaramos el pin del foco de la sala       
  pinMode(garaje,OUTPUT);            // Declaramos el pin de la puerta del garaje
  pinMode(cuarto,OUTPUT);            // Declaramos el pin del foco del cuarto
  pinMode(azotea,OUTPUT);            // Declaramos el pin del foco de la azotea
  pinMode(cocina,OUTPUT);            // Declaramos el pin del foco de la cocina
  pinMode(motor,OUTPUT);             // Declaramos el pin del motor
  pinMode(cochera,OUTPUT);
  Serial.begin(9600);                // Colocamos la comunicacion serial a 9600 bits
  delay(1000);
 }
 void loop(){
 if(Serial.available()>0){           // Si el puerto serie esta habilitado 
        estado = Serial.read();      // Lee lo que llega por el puerto Serie
 }
     
 if(estado== 'a'){                    // on/off de los pines de los instrumes a controlar 
       digitalWrite(sala,HIGH);
 }
 if(estado== 'b' ){
       digitalWrite(sala,LOW);
 }
 if(estado== 'c'){ 
       digitalWrite(garaje,HIGH);
 }
 if(estado== 'd' ){
       digitalWrite(garaje,LOW);
 }
 if(estado== 'e'){ 
       digitalWrite(cuarto,HIGH);
 }
 if(estado== 'f' ){
       digitalWrite(cuarto,LOW);
 }
 if(estado== 'g' ){
       digitalWrite(azotea,HIGH);
 }
 if(estado== 'h' ){
       digitalWrite(azotea,LOW);
 }
 if(estado== 'i' ){
        digitalWrite(cocina,HIGH);
 }
 if(estado== 'j' ){
       digitalWrite(cocina,LOW);
 }
if(estado== 'k' ){
        digitalWrite(cochera,HIGH);
 }
 if(estado== 'l' ){
       digitalWrite(cochera,LOW);
      }

 if(estado== 'A' ){      // Movimiento del servo
      gargateservo.write(0);  
      }
 if(estado== 'B' ){
      gargateservo.write(90); 
      }
 if(estado== 'C' ){
      gargateservo.write(180);  
      }
 
 if(estado=='s'){       //  Controlamos el motor de forma analofica con valores en 0 a 255
        analogWrite(motor,0);
       }
      
 if(estado=='t'){
        analogWrite(motor,3);
       }   
if(estado=='u'){
        analogWrite(motor,8);
       }  
if(estado=='v'){
        analogWrite(motor,20);
       }  
if(estado=='w'){
        analogWrite(motor,30);
       }  
if(estado=='x'){
        analogWrite(motor,60);
       }  
if(estado=='y'){
        analogWrite(motor,125);
       }  
if(estado=='z'){
        analogWrite(motor,255);
       }         
       
if(estado=='q'){         // envia el valor leido del puerto analogo A0
  analogo=analogRead(A0);
  Serial.print(analogo);
  Serial.println("∞C");
  delay (tiempo);
  estado=0;
 }
 delay(tiempo);
 }
 
 
